return {
    'bluz71/vim-nightfly-guicolors',
    name = 'nightfly',
    config = function()
        vim.g.nightflyWinSeparator = 2
    end,
}
