return {
    'nvim-telescope/telescope.nvim',
    tag = '0.1.5',
    name = 'telescope',
    dependencies = { 'nvim-lua/plenary.nvim', name = 'plenary', },
    keys = {
        {'<leader><leader>', '<cmd>Telescope<cr>', desc = 'telescope'},
        {'<leader>ff', '<cmd>Telescope find_files<cr>', desc = 'telescope find files'},
        {'<leader>fh', '<cmd>Telescope oldfiles<cr>', desc = 'telescope history'},
        {'<leader>fr', '<cmd>Telescope live_grep<cr>', desc = 'telescope ripgrep'},
        {'<leader>fc', '<cmd>Telescope git_commits<cr>', desc = 'telescope commits'},
        {'<leader>fg', '<cmd>Telescope git_files<cr>', desc = 'telescope git files'},
    },

    config = function()
        -- use <esc> to quit directly from insert mode
        -- the default is to enter normal mode
        local actions = require('telescope.actions')
        require('telescope').setup{
            defaults = {
                mappings = {
                    i = {
                        ['<esc>'] = actions.close
                    },
                },
            }
        }
    end,
}
