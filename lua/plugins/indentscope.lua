return {
    'echasnovski/mini.indentscope',
    version = false,
    name = 'indentscope',
    config = function ()
        require('mini.indentscope').setup({
            symbol = "│",
        })
    end
}
