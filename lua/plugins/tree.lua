  return {
  "nvim-tree/nvim-tree.lua",
  dependencies = { "nvim-tree/nvim-web-devicons", name = 'devicons' },
  name = 'tree',
  config = true,
  keys = {
      {'<leader>ee', '<cmd>NvimTreeToggle<cr>'},
  },
}
