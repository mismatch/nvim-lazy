return{
    'hrsh7th/nvim-cmp',
    name = 'cmp',
    event = 'InsertEnter',
    dependencies = {
        {'hrsh7th/cmp-buffer'},
        {'hrsh7th/cmp-path'},
        {'L3MON4D3/LuaSnip', name = 'cmp-luasnip'},
        {'hrsh7th/cmp-calc'},
        {'hrsh7th/cmp-nvim-lsp'},
        {'hrsh7th/cmp-nvim-lua'},
        {'hrsh7th/cmp-emoji'},
        {'hrsh7th/cmp-nvim-lsp-signature-help'},
        {'f3fora/cmp-spell'},
        {'onsails/lspkind-nvim'},
        {'kdheepak/cmp-latex-symbols'},
    },
    config = function()
        local cmp = require('cmp')
        local luasnip = require('luasnip')
        local check_back_space = function()
            local col = vim.fn.col '.' - 1
            return col == 0 or vim.fn.getline('.'):sub(col, col):match '%s' ~= nil
        end
        local t = function(str)
            return vim.api.nvim_replace_termcodes(str, true, true, true)
        end

        cmp.setup({
            completion = {
                completeopt = 'menu,menuone,preview,noselect',
            },
            snippet = {
                expand = function(args)
                    require('luasnip').lsp_expand(args.body)
                end
            },
            sources = {
                { name = 'nvim_lsp'},
                { name = 'buffer', keyword_length = 3},
                { name = 'path'},
                { name = 'luasnip'},
                { name = 'spell', keyword_length = 5},
                { name = 'calc'},
                { name = 'nvim_lsp_signature_help'},
                { name = 'nvim_lua'},
                -- { name = 'latex_symbols', priority = 1, max_item_count = 5},
                { name = 'emoji'},
            },
            mapping = {
                ["<tab>"] = cmp.mapping(function(fallback)
                    if cmp.visible() then
                        cmp.select_next_item()
                    elseif luasnip.expand_or_jumpable() then
                        vim.fn.feedkeys(t("<plug>luasnip-expand-or-jump"), "")
                    elseif check_back_space() then
                        vim.fn.feedkeys(t("<tab>"), "n")
                    else
                        fallback()
                    end
                end, {
                "i",
                "s",
            }),
            ["<s-tab>"] = cmp.mapping(function(fallback)
                if cmp.visible() then
                    cmp.select_prev_item()
                elseif luasnip.jumpable(-1) then
                    vim.fn.feedkeys(t("<plug>luasnip-jump-prev"), "")
                else
                    fallback()
                end
            end, {
            "i",
            "s",
        }),
        ['<c-k>'] = cmp.mapping(cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Select }), {'i'}),
        ['<c-j>'] = cmp.mapping(cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Select }), {'i'}),
        ['<c-d>'] = cmp.mapping.scroll_docs(-4),
        ['<c-f>'] = cmp.mapping.scroll_docs(4),
        ['<c-Space>'] = cmp.mapping.complete(),
        ['<c-e>'] = cmp.mapping.close(),
        ['<cr>'] = cmp.mapping.confirm({
            behavior = cmp.ConfirmBehavior.Replace,
            select = true,
        })
    },
})
    end,
}
