return {
    'neovim/nvim-lspconfig',
    name = 'lspconfig',
    event = {'BufReadPre', 'BufNewFile'},
    dependencies = {
        'hrsh7th/cmp-nvim-lsp',
    },

    config = function()
        local lspconfig = require('lspconfig')
        local cmp_nvim_lsp = require('cmp_nvim_lsp')
        local capabilities = cmp_nvim_lsp.default_capabilities()
        local on_attach = function(client, bufnr) end


        -- diagnostic signs in the gutter
        local signs = { Error = ' ', Warn = ' ', Hint = '󰠠 ', Info = ' ' }
        for type, icon in pairs(signs) do
            local hl = 'DiagnosticSign' .. type
            vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
        end

        -- configure html server
        lspconfig['html'].setup({
            capabilities = capabilities,
            on_attach = on_attach,
        })

        -- configure lua ls
        lspconfig['lua_ls'].setup({
            settings = {
                Lua = {
                    runtime = {
                        version = 'LuaJIT'
                    },
                    -- make the server aware of Neovim runtime files
                    workspace = {
                        checkThirdParty = false,
                        library = {
                            vim.env.VIMRUNTIME
                        }
                    }
                }
            }
        })

        -- configure pyright server
        lspconfig['pyright'].setup({
            capabilities = capabilities,
            on_attach = on_attach,
        })
    end,
}
