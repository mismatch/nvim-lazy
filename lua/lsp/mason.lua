return {
    'williamboman/mason.nvim',
    name = 'mason',
    dependencies = {
        {'williamboman/mason-lspconfig.nvim', name = 'mason-lspconfig'},
    },
    config = function()
        local mason = require('mason')
        local mason_lspconfig = require('mason-lspconfig')

        -- load mason before mason-lspconfig
        mason.setup()
        mason_lspconfig.setup({
            -- list of servers for mason to install
            ensure_installed = {
                'html',
                'pyright',
                'lua_ls',
                'texlab',
            },
            -- auto-install configured servers (with lspconfig)
            automatic_installation = true,
        })
    end,
}
