-- mappings needs to be loaded first for leader key
require('settings.mappings')
require('settings.options')
require('settings.statusline')

-- lazy requires all items from 
-- 'lua/plugins'
-- 'lua/colours'
-- 'lua/lsp'
require('settings.lazy')
